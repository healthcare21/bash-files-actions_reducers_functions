import { 
    SET_DISCIPLINES,
    
} from '../actions/types'

const initialState = {
    disciplines: [],
}

export default function(state = initialState, action){
    switch (action.type) {
        
        case SET_DISCIPLINES:
         
            return {
                ...state,
                disciplines: action.payload,

            }
        
        default:
            return state;
    }

}