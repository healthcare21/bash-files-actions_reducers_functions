import {
   SET_DISCIPLINES,
 
} from './types'

export const setDisciplines= (data) => {
    // document.title = data.titles.primary
    return {
        type: SET_DISCIPLINES,
        payload: data
    }
}